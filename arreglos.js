//Creo arreglo
console.log("creo arreglo");
var frutas = ["pera","manzana","banana"];
console.log(frutas);

//Creo arreglo como objeto
console.log("Creo arreglo como objeto");
var arregloFrutas = new Array ("uva", "kiwi", "sandia");
console.log(arregloFrutas);

//Obtengo cantidad de frutas
console.log("Obtengo cantidad de frutas");
cantFrutas = arregloFrutas.length;
console.log(cantFrutas);

//Accedo a una posicion del arreglo
console.log("Accedo a la posicion n° 1");
console.log(arregloFrutas[1]);

//Agrega el elemento al principio
console.log("Agrego una fruta al inicio y al final");
arregloFrutas.unshift("naranja"); 

// Agrega elemento al final
arregloFrutas.push("frutilla");
console.log(arregloFrutas);

//**********BUSQUEDA**********/
//Buscamos el elemento
console.log("Busqueda de kiwi");
var indice = arregloFrutas.indexOf("kiwi");
//Obtenemos el elemento buscado
var unaFruta = arregloFrutas[indice];
console.log(unaFruta);

//**********Eliminacion**********/
//Eliminamos el primer elemento
console.log("Elimino primer y ultimo elemento");
arregloFrutas.shift();
//Eliminamos el último elemento
arregloFrutas.pop();
console.log(arregloFrutas);

//Eliminamos un elemento en particular
//El primer parametro indica el inicio y el segundo la cantidad de elementos a eliminar
console.log("Elimino elemento en posicion 0");
arregloFrutas.splice(0, 1);
console.log(arregloFrutas);

//Recorriendo el arreglo con FOR
console.log("Recorriendo con FOR");
for (var i = 0; i < arregloFrutas.length; i++) {
    console.log(arregloFrutas[i]);
}

//Recorriendo el arreglo con WHILE
console.log("Recorriendo con WHILE");
var i = 0;
while (i < arregloFrutas.length) {
    console.log(arregloFrutas[i]);
    i++;
}